/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.Lab9.beans.impl;

import java.util.Date;
import pk.labs.Lab9.beans.Term;

/**
 *
 * @author st
 */
public class TermIMPL implements Term{
    private Date begin;
    private int duration;
    
    public TermIMPL(){}
    @Override
    public Date getBegin() {
        return this.begin;
    }

    @Override
    public void setBegin(Date begin) {
        this.begin = begin;
        
    }

    @Override
    public int getDuration() {
        return duration; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setDuration(int duration) {
        if(duration >0)
        this.duration = duration;
        else
            this.duration = 30;//To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Date getEnd() {
        return  new Date(this.begin.getTime() + this.duration*60000);
    }
    
}
