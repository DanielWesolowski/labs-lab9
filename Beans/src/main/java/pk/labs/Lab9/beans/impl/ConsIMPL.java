/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.Lab9.beans.impl;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeSupport;
import java.util.Date;
import pk.labs.Lab9.beans.Consultation;
import pk.labs.Lab9.beans.Term;

/**
 *
 * @author st
 */
public class ConsIMPL implements Consultation{
    private String student;
    private Term term;
    
    private final VetoableChangeSupport veto = new VetoableChangeSupport(this);
    private final PropertyChangeSupport prop = new PropertyChangeSupport(this);
    public ConsIMPL(){}
    
        
    public void removePropertyChangeListener(PropertyChangeListener listener) { this.prop.removePropertyChangeListener(listener); }

    
    public void addPropertyChangeListener(PropertyChangeListener listener) { this.prop.addPropertyChangeListener(listener); }
    
    
    @Override
    public String getStudent() { return this.student;   }

    @Override
    public void setStudent(String student) { this.student = student;    }

    @Override
    public Date getBeginDate() { return this.term.getBegin();  }

    @Override
    public Date getEndDate() { return this.term.getEnd();    }

    @Override
    public void setTerm(Term term) throws PropertyVetoException { Term staryTermin = this.term;
    veto.fireVetoableChange("Term", staryTermin, term);
    this.term = term;
    prop.firePropertyChange("Term", staryTermin, term);}

    @Override
    public void prolong(int minutes) throws PropertyVetoException { 
    if(minutes <= 0) minutes = 0;
    int staryPrzedzial = this.term.getDuration();
    this.veto.fireVetoableChange("Term", staryPrzedzial, staryPrzedzial + minutes);
    this.term.setDuration(this.term.getDuration()+minutes);
    this.prop.firePropertyChange("Term", staryPrzedzial, staryPrzedzial + minutes);
    }
    
}
