/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.Lab9.beans.impl;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.util.Arrays;
import pk.labs.Lab9.beans.Consultation;
import pk.labs.Lab9.beans.ConsultationList;

/**
 *
 * @author st
 */
public class ListaKonsultacjiIMPL implements ConsultationList{
       
    private final PropertyChangeSupport prop = new PropertyChangeSupport(this);
    
    private Consultation[] listaKonsultacji;
    
    public ListaKonsultacjiIMPL(){
    this.listaKonsultacji = new Consultation[]{};
    }
    @Override
    public int getSize() {return this.listaKonsultacji.length;}

    @Override
    public Consultation[] getConsultation() { return this.listaKonsultacji;  }

    @Override
    public Consultation getConsultation(int index) {return this.listaKonsultacji[index];}

    @Override
    public void addConsultation(Consultation consultation) throws PropertyVetoException 
    {
        for(int i=0; i < this.listaKonsultacji.length;i++)
        { if((consultation.getEndDate().after(this.listaKonsultacji[i].getBeginDate()) && consultation.getBeginDate().before(this.listaKonsultacji[i].getBeginDate())) ||
                (consultation.getBeginDate().before(this.listaKonsultacji[i].getEndDate()) && consultation.getEndDate().after(this.listaKonsultacji[i].getBeginDate())))
        { throw new PropertyVetoException("Error", null); } 
        } 
        Consultation[] nowa = Arrays.copyOf(this.listaKonsultacji, this.listaKonsultacji.length + 1); 
        Consultation[] stara = this.listaKonsultacji; 
        nowa[nowa.length - 1] = consultation; this.listaKonsultacji = nowa; 
        prop.firePropertyChange("consultation", stara, nowa);
    }
    

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) { this.prop.removePropertyChangeListener(listener); }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) { this.prop.addPropertyChangeListener(listener); }
    
}
