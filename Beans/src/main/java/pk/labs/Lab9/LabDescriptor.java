package pk.labs.Lab9;

import pk.labs.Lab9.beans.*;
import pk.labs.Lab9.beans.impl.ConsIMPL;
import pk.labs.Lab9.beans.impl.FabrykaKonsIMPL;
import pk.labs.Lab9.beans.impl.ListaKonsultacjiIMPL;
import pk.labs.Lab9.beans.impl.TermIMPL;

public class LabDescriptor {
    
    public static Class<? extends Term> termBean = TermIMPL.class;
    public static Class<? extends Consultation> consultationBean = ConsIMPL.class;
    public static Class<? extends ConsultationList> consultationListBean = ListaKonsultacjiIMPL.class;
    public static Class<? extends ConsultationListFactory> consultationListFactory = FabrykaKonsIMPL.class;
    
}
